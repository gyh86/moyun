/*
 Navicat MySQL Data Transfer

 Source Server         : native
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : moyun

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 02/07/2018 15:15:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_columns
-- ----------------------------
DROP TABLE IF EXISTS `gen_columns`;
CREATE TABLE `gen_columns`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `t_id` bigint(20) NULL DEFAULT NULL COMMENT '表的ID',
  `jdbc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据库中字段名',
  `java_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'java属性名',
  `comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据库中的注释',
  `jdbc_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据库中的字段类型',
  `java_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'java中的属性类型',
  `is_pk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否是主键',
  `is_null` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为空',
  `is_insert` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1：插入字段）',
  `is_query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否用于查询条件',
  `is_edit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1：编辑字段）',
  `query_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '查询类型',
  `length` int(11) NULL DEFAULT NULL COMMENT '字段的长度',
  `dict_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字典类型',
  `sort` int(255) NULL DEFAULT NULL COMMENT '排序（升序）',
  `create_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '修改者',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `del` enum('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '生成表时保存的字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_columns
-- ----------------------------
INSERT INTO `gen_columns` VALUES (36, 5, 'id', 'id', '编号', 'bigint(20)', 'Long', 'PRI', 'NO', NULL, NULL, NULL, NULL, 20, NULL, 1, NULL, '2018-06-30 10:51:33', NULL, '2018-06-30 10:51:33', NULL, 'N');
INSERT INTO `gen_columns` VALUES (37, 5, 'name', 'name', '名字', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 2, NULL, '2018-06-30 10:51:34', NULL, '2018-06-30 10:51:34', NULL, 'N');
INSERT INTO `gen_columns` VALUES (38, 5, 'age', 'age', '年龄', 'int(11)', 'Integer', '', 'YES', NULL, NULL, NULL, NULL, 11, NULL, 3, NULL, '2018-06-30 10:51:34', NULL, '2018-06-30 10:51:34', NULL, 'N');
INSERT INTO `gen_columns` VALUES (39, 5, 'create_date', 'createDate', '创建时间', 'datetime', 'Date', '', 'YES', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, '2018-06-30 10:51:34', NULL, '2018-06-30 10:51:34', NULL, 'N');
INSERT INTO `gen_columns` VALUES (40, 5, 'del', 'del', '删除标记', 'enum(\'Y\',\'N\')', 'String', '', 'NO', NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, '2018-06-30 10:51:35', NULL, '2018-06-30 10:51:35', NULL, 'N');
INSERT INTO `gen_columns` VALUES (41, 6, 'id', 'id', '编号', 'bigint(20)', 'Long', 'PRI', 'NO', NULL, NULL, NULL, NULL, 20, NULL, 1, NULL, '2018-06-30 17:44:03', NULL, '2018-06-30 17:44:03', NULL, 'N');
INSERT INTO `gen_columns` VALUES (42, 6, 'jdbc_name', 'jdbcName', '数据库表名', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 2, NULL, '2018-06-30 17:44:05', NULL, '2018-06-30 17:44:05', NULL, 'N');
INSERT INTO `gen_columns` VALUES (43, 6, 'java_name', 'javaName', '实体类名', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 3, NULL, '2018-06-30 17:44:06', NULL, '2018-06-30 17:44:06', NULL, 'N');
INSERT INTO `gen_columns` VALUES (44, 6, 'comments', 'comments', '注释信息', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 4, NULL, '2018-06-30 17:44:06', NULL, '2018-06-30 17:44:06', NULL, 'N');
INSERT INTO `gen_columns` VALUES (45, 6, 'parent_table', 'parentTable', '关联父表', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 5, NULL, '2018-06-30 17:44:06', NULL, '2018-06-30 17:44:06', NULL, 'N');
INSERT INTO `gen_columns` VALUES (46, 6, 'parent_table_fk', 'parentTableFk', '关联父表外键', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 6, NULL, '2018-06-30 17:44:07', NULL, '2018-06-30 17:44:07', NULL, 'N');
INSERT INTO `gen_columns` VALUES (47, 6, 'create_by', 'createBy', '创建者', 'bigint(20)', 'Long', '', 'YES', NULL, NULL, NULL, NULL, 20, NULL, 7, NULL, '2018-06-30 17:44:07', NULL, '2018-06-30 17:44:07', NULL, 'N');
INSERT INTO `gen_columns` VALUES (48, 6, 'create_time', 'createTime', '创建时间', 'datetime', 'Date', '', 'NO', NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, '2018-06-30 17:44:07', NULL, '2018-06-30 17:44:07', NULL, 'N');
INSERT INTO `gen_columns` VALUES (49, 6, 'update_by', 'updateBy', '修改者', 'bigint(20)', 'Long', '', 'YES', NULL, NULL, NULL, NULL, 20, NULL, 9, NULL, '2018-06-30 17:44:08', NULL, '2018-06-30 17:44:08', NULL, 'N');
INSERT INTO `gen_columns` VALUES (50, 6, 'update_time', 'updateTime', '修改时间', 'datetime', 'Date', '', 'NO', NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, '2018-06-30 17:44:08', NULL, '2018-06-30 17:44:08', NULL, 'N');
INSERT INTO `gen_columns` VALUES (51, 6, 'remarks', 'remarks', '备注', 'varchar(255)', 'String', '', 'YES', NULL, NULL, NULL, NULL, 255, NULL, 11, NULL, '2018-06-30 17:44:08', NULL, '2018-06-30 17:44:08', NULL, 'N');
INSERT INTO `gen_columns` VALUES (52, 6, 'del', 'del', '删除标记', 'enum(\'Y\',\'N\')', 'String', '', 'NO', NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL, '2018-06-30 17:44:09', NULL, '2018-06-30 17:44:09', NULL, 'N');

-- ----------------------------
-- Table structure for gen_schemes
-- ----------------------------
DROP TABLE IF EXISTS `gen_schemes`;
CREATE TABLE `gen_schemes`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `t_id` bigint(20) NULL DEFAULT NULL COMMENT '表ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模板名称',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分类',
  `package_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块名',
  `function_author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `function_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块描述',
  `subModule_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名（简写）',
  `replace_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '0：保存方案； 1：保存方案并生成代码',
  `flag` tinyint(4) NULL DEFAULT NULL COMMENT ' 是否替换现有文件 0：不替换；1：替换文件',
  `create_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '修改者',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `del` enum('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '对像生成的方案' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_schemes
-- ----------------------------
INSERT INTO `gen_schemes` VALUES (2, 5, '测试数据', '', 'com.gyh.modules', 'testss', 'gyh', '测试数据表', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-30 17:44:00', NULL, '2018-06-30 17:44:00', NULL, 'N');

-- ----------------------------
-- Table structure for gen_tables
-- ----------------------------
DROP TABLE IF EXISTS `gen_tables`;
CREATE TABLE `gen_tables`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `jdbc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据库表名',
  `java_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '实体类名',
  `comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '注释信息',
  `parent_table` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联父表',
  `parent_table_fk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联父表外键',
  `create_by` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) NULL DEFAULT NULL COMMENT '修改者',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `del` enum('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据库中生成的表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_tables
-- ----------------------------
INSERT INTO `gen_tables` VALUES (5, 'test_app', 'TestApp', '测试数据', NULL, NULL, NULL, '2018-06-30 10:51:33', NULL, '2018-06-30 10:51:33', NULL, 'N');
INSERT INTO `gen_tables` VALUES (6, 'gen_tables', 'GenTables', '数据库中生成的表', NULL, NULL, NULL, '2018-06-30 17:43:53', NULL, '2018-06-30 17:43:53', NULL, 'N');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `company_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '归属公司',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '归属部门',
  `login_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '登录名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `no` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '工号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `mobile` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机',
  `user_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户类型',
  `photo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户头像',
  `login_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `login_flag` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否可登录',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_user_office_id`(`office_id`) USING BTREE,
  INDEX `sys_user_login_name`(`login_name`) USING BTREE,
  INDEX `sys_user_company_id`(`company_id`) USING BTREE,
  INDEX `sys_user_update_date`(`update_date`) USING BTREE,
  INDEX `sys_user_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for test_app
-- ----------------------------
DROP TABLE IF EXISTS `test_app`;
CREATE TABLE `test_app`  (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名字',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `del` enum('Y','N') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'N' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '测试数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test_app
-- ----------------------------
INSERT INTO `test_app` VALUES (1, '网名', 1, '2018-06-28 19:54:57', 'N');

SET FOREIGN_KEY_CHECKS = 1;
