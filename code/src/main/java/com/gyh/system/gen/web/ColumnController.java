package com.gyh.system.gen.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.common.persistence.BaseController;
import com.gyh.system.common.persistence.Page;
import com.gyh.system.common.utils.R;
import com.gyh.system.gen.entity.Columns;
import com.gyh.system.gen.service.ColumnsService;
import com.gyh.system.gen.utils.ModelAttr;

/**
* @author 作者 gyh:
* @version 创建时间：2018年5月2日 下午10:14:22
* 
*/
@RestController
@CrossOrigin
@RequestMapping("/column")
public class ColumnController  extends BaseController{

	@Autowired
	private ColumnsService columnsService;
	
	@GetMapping("database")
	public R getTables(String table) {
		return R.ok(columnsService.searchColumns(table));
	}
	
	@GetMapping()
	public R getTables(Columns column) {
		return R.ok(columnsService.findList(column));
	}
	
	@GetMapping("page")
	public R getTables(Columns column,@RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
		Page<Columns> page = columnsService.findPage(new Page<Columns>(pageNo,pageSize), column);
		return R.ok(page);
	}
	
	
	@PostMapping
	public R save(@RequestBody ModelAttr modelAttr) {
		columnsService.addTableAndColumn(modelAttr);
		return R.ok();
	}
	
}
