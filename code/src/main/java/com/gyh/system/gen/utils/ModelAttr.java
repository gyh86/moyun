package com.gyh.system.gen.utils;

import java.util.ArrayList;
import java.util.List;

import com.gyh.system.gen.entity.Columns;
import com.gyh.system.gen.entity.Tables;

/**
 * 
 * @author 作者 gyh
 * @version 创建时间：2018年6月30日 下午5:55:52
 */
public class ModelAttr {
	
	private Tables table;
	private List<Columns> columns = new ArrayList<>();
	
	
	
	public Tables getTable() {
		return table;
	}
	public void setTable(Tables table) {
		this.table = table;
	}
	public List<Columns> getColumns() {
		return columns;
	}
	public void setColumns(List<Columns> columns) {
		this.columns = columns;
	}
	
	
	
}
