package com.gyh.system.gen.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.common.persistence.BaseController;
import com.gyh.system.common.utils.R;
import com.gyh.system.gen.entity.Schemes;
import com.gyh.system.gen.service.SchemesService;
import com.gyh.system.gen.utils.VelocityUtils;

/**
* @author 作者 gyh:
* @version 创建时间：2018年4月26日 下午12:22:01
* 
*/
@RestController
@RequestMapping(value="/schemes")
@CrossOrigin
public class SchemesController extends BaseController{

	@Autowired
	private SchemesService schemesService;
	
	
	@GetMapping
	public R searchList(Schemes entity) {
		System.out.println(entity.getModuleName());
		return R.ok(schemesService.findList(entity));
	}
	
	@GetMapping("search")
	public R searchSchemeAndTable(Schemes schemes) {
		return R.ok(schemesService.searchListAndTable(schemes));
	}
	
	
	@GetMapping("/gen")
	public R generateCode(Long id) {
		
		Schemes scheme = schemesService.searchEnity(id);
		VelocityUtils.createFile(scheme);
		schemesService.update(new Schemes(id,"1"));
		return R.ok();
	}
	
	@PostMapping
	public R save(@RequestBody Schemes entity) {
		schemesService.save(entity);
		return R.ok();
	}
	
	@GetMapping("delete")
	public R delete(Long id) {
		return R.ok(schemesService.deleteAlways(id));
	}
	
}
