package com.gyh.system.gen.entity;

import com.gyh.system.common.persistence.BaseEntity;
import com.mysql.cj.xdevapi.Table;

/**
* @author 作者 gyh:
* @version 创建时间：2018年2月4日 下午9:08:32
* 
*/
public class Schemes extends BaseEntity<Schemes> {
	private static final long serialVersionUID = 1L;
	
	private Long tId;//表ID
	private String name;//模板名称
	private String category;//分类
	private String packageName;//生成包路径
	private String moduleName;//模块名
	private String functionAuthor;//创建者
	private String functionComment;//模块描述
	private String subModuleName; // 生成子模块名
	private String functionName; // 生成功能名
	private String functionNameSimple; // 生成功能名（简写）
	private String flag; // 0：未生成代码； 1：生成代码
	private boolean replaceFile; // 是否替换现有文件 0：不替换；1：替换文件
	
	private Tables table; 
	
	
	
	public Schemes() {
		super();
	}
	
	public Schemes(Long id,String flag) {
		super(id);
		this.flag = flag;
	}
	public Long gettId() {
		return tId;
	}
	public void settId(Long tId) {
		this.tId = tId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getFunctionAuthor() {
		return functionAuthor;
	}
	public void setFunctionAuthor(String functionAuthor) {
		this.functionAuthor = functionAuthor;
	}
	public String getFunctionComment() {
		return functionComment;
	}
	public void setFunctionComment(String functionComment) {
		this.functionComment = functionComment;
	}
	public Tables getTable() {
		return table;
	}
	public void setTable(Tables table) {
		this.table = table;
	}
	public String getSubModuleName() {
		return subModuleName;
	}
	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFunctionNameSimple() {
		return functionNameSimple;
	}
	public void setFunctionNameSimple(String functionNameSimple) {
		this.functionNameSimple = functionNameSimple;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public boolean getReplaceFile() {
		return replaceFile;
	}
	public void setReplaceFile(boolean replaceFile) {
		this.replaceFile = replaceFile;
	}
	
}
