package com.gyh.system.gen.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月20日 下午9:28:16
* 
*/
public class Tables extends BaseEntity<Tables>{

	private static final long serialVersionUID = 1L;

	private String jdbcName;		//数据库表名
	private String javaName;		//实体类名
	private String comments;		//注释信息
	private String parentTable;		//关联父表
	private String parentTableFk;	//关联父表外键
	
	private Tables parent;//父表对象
	private List<Columns> list = new ArrayList<>();
	
	
	public String getJdbcName() {
		return jdbcName;
	}
	public void setJdbcName(String jdbcName) {
		this.jdbcName = jdbcName;
	}
	public String getJavaName() {
		return javaName;
	}
	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getParentTable() {
		return parentTable;
	}
	public void setParentTable(String parentTable) {
		this.parentTable = parentTable;
	}
	public String getParentTableFk() {
		return parentTableFk;
	}
	public void setParentTableFk(String parentTableFk) {
		this.parentTableFk = parentTableFk;
	}
	public Tables getParent() {
		return parent;
	}
	public void setParent(Tables parent) {
		this.parent = parent;
	}
	public List<Columns> getList() {
		return list;
	}
	public void setList(List<Columns> list) {
		this.list = list;
	}
	
}
