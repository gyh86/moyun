package com.gyh.system.gen.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.common.tools.StringUtils;
import com.gyh.system.gen.dao.TablesDao;
import com.gyh.system.gen.entity.Tables;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月22日 下午11:11:21
* 
*/
@Service
public class TablesService extends BaseService<TablesDao, Tables>{

	@Autowired
	private TablesDao tablesDao;
	
	public Tables searchEntity(Tables entity) {
		return tablesDao.searchEntity(entity);
	}
	
	/**
	 * 获取本数据库中所有的表
	 * @return
	 */
	public List<Tables> searchTables(){
		List<Tables> tables = tablesDao.searchTables();
		for (Tables table : tables) {
			if (StringUtils.isBlank(table.getJavaName())) {
				table.setJavaName(StringUtils.initialCaseToUpper(table.getJdbcName()));
			}
		}
		return tables;
	}
	
}
