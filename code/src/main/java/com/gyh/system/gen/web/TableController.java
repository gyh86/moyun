package com.gyh.system.gen.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.common.persistence.BaseController;
import com.gyh.system.common.utils.R;
import com.gyh.system.gen.entity.Tables;
import com.gyh.system.gen.service.TablesService;

/**
* @author 作者 gyh:
* @version 创建时间：2018年5月2日 下午10:14:22
* 
*/
@RestController
@CrossOrigin
@RequestMapping("/table")
public class TableController  extends BaseController{

	@Autowired
	private TablesService tablesService;
	
	/**
	 * 获取已经配置多字段的表
	 * @return
	 */
	@GetMapping
	public R getTableFinish() {
		return R.ok(tablesService.findList(new Tables()));
	}
	
	
	
	/**
	 * 获取去数据库中所有的表
	 * @return
	 */
	@GetMapping("database")
	public R getTables() {
		return R.ok(tablesService.searchTables());
	}
	
	
	@GetMapping("getAll")
	public R getALLList(Tables tables) {
		List<Tables> table = tablesService.findList(tables);
		return R.ok(table);
	}
	
	@PostMapping
	public R save(@RequestBody Tables table) {
		tablesService.save(table);
		return R.ok("保存成功");
	}
	
	@PostMapping("save")
	public R saves(@RequestBody List<Tables> list) {
		for (Tables table : list) {
			tablesService.save(table);
		}
		return R.ok("保存成功");
	}
	
	@GetMapping("delete")
	public R delete(Long id) {
		tablesService.delete(id);
		return R.ok("删除成功");
	}
	
	@GetMapping("deleteAlway")
	public R deleteAlways(Long id) {
		tablesService.deleteAlways(id);
		return R.ok("已永久删除");
	}
	
	
}
