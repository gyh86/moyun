package com.gyh.system.gen.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.gen.dao.SchemesDao;
import com.gyh.system.gen.entity.Schemes;

/**
* @author 作者 gyh:
* @version 创建时间：2018年2月4日 下午9:17:25
* 
*/
@Service
public class SchemesService extends BaseService<SchemesDao, Schemes> {

	@Autowired
	private SchemesDao schemesDao;
	
	public Schemes searchEnity(Long schemeId) {
		return schemesDao.searchEnity(schemeId);
	}
	
	/**
	 * 获取schemes和table
	 * @param schemes
	 * @return
	 */
	public List<Schemes> searchListAndTable(Schemes schemes){
		return schemesDao.searchListAndTable(schemes);
	}
}
