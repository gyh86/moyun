package com.gyh.system.gen.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.gyh.system.common.utils.Global;
import com.gyh.system.gen.entity.Schemes;

/**
* @author 作者 gyh:
* @version 创建时间：2018年2月3日 下午5:29:55
* 
*/
public class VelocityUtils {
	
	public static final String VM_ENTITY="template/velocity/entity.vm";
	public static final String VM_DAO="template/velocity/dao.vm";
	public static final String VM_DAO_MAPPER="template/velocity/mapper.vm";
	public static final String VM_SERVICE="template/velocity/service.vm";
	public static final String VM_CONTROLLER="template/velocity/controller.vm";
	
	
	/**
	 * 创建数据-文件
	 */
	public static void createFile(Schemes scheme) {
		VelocityContext ctx = new VelocityContext();
		ctx.put("scheme", scheme);
		//添加实体类数据模板
		Template entityTemplate = VelocityUtils.getTemplate(VM_ENTITY);
		String entityPath="java/"+StringUtils.replaceChars(scheme.getPackageName(), '.', '/')+"/"+scheme.getModuleName()+"/entity";
		merge(entityTemplate, ctx, entityPath,StringUtils.capitalize(scheme.getTable().getJavaName())+".java");
		//添加Dao数据模板
		Template daoTemplate = VelocityUtils.getTemplate(VM_DAO);
		String daoPath="java/"+StringUtils.replaceChars(scheme.getPackageName(), '.', '/')+"/"+scheme.getModuleName()+"/dao";
		merge(daoTemplate, ctx, daoPath ,StringUtils.capitalize(scheme.getTable().getJavaName())+"Dao.java");
		//添加映射文件数据模板
		Template mapperTemplate = VelocityUtils.getTemplate(VM_DAO_MAPPER);
		String mapperPath="resources/mapper/"+StringUtils.substringAfterLast(scheme.getPackageName(), ".")+"/"+scheme.getModuleName();
		merge(mapperTemplate, ctx, mapperPath,StringUtils.capitalize(scheme.getTable().getJavaName())+"Dao.xml");
		//添加service数据模板
		Template serviceTemplate = VelocityUtils.getTemplate(VM_SERVICE);
		String servicePath="java/"+StringUtils.replaceChars(scheme.getPackageName(), '.', '/')+"/"+scheme.getModuleName()+"/service";
		merge(serviceTemplate, ctx, servicePath,StringUtils.capitalize(scheme.getTable().getJavaName())+"Service.java");
		//添加controller数据模板
		Template controllerTemplate = VelocityUtils.getTemplate(VM_CONTROLLER);
		String controllerPath="java/"+StringUtils.replaceChars(scheme.getPackageName(), '.', '/')+"/"+scheme.getModuleName()+"/web";
		merge(controllerTemplate, ctx, controllerPath,StringUtils.capitalize(scheme.getTable().getJavaName())+"Controller.java");
	}

	/**
	 * 创建模板
	 * @param template 模板路基以及名称
	 * @return 模板对象
	 */
	public static Template getTemplate(String template) {
		 VelocityEngine ve = new VelocityEngine();
		 ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		 ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		 ve.setProperty(Velocity.ENCODING_DEFAULT, "UTF-8");
		 ve.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
		 ve.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
		 ve.init();
		 Template t = ve.getTemplate(template);
		 t.setEncoding("UTF-8");
		 return t;
	}
	
	/**
	 * 为模板赋值参数
	 * @param template 模板
	 * @param ctx 
	 * @param path 路径
	 */
	public static void merge(Template template, VelocityContext ctx, String path ,String name) {
		String rootPath = Global.PATH_PROJECT+"/src/main/";
		PrintWriter writer = null;
		File file = null;
		try {
			file = new File(rootPath+path);
			if(!file.exists()) file.mkdirs();
			writer = new PrintWriter(file+"/"+name,"UTF-8");
			StringWriter sw = new StringWriter();
			template.merge(ctx, writer);
			template.merge(ctx, sw);
			writer.flush();
			//System.out.println(sw);
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
	
}
