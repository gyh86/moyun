package com.gyh.system.gen.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;
import com.gyh.system.gen.entity.Schemes;

/**
* @author 作者 gyh:
* @version 创建时间：2018年2月4日 下午9:15:25
* 
*/
@MyBatisDao
public interface SchemesDao extends BaseDao<Schemes>{

	/**
	 * 高级查询
	 * @param schemes
	 * @return
	 */
	Schemes searchEnity(@Param("schemeId")Long schemeId);
	
	/**
	 * 获取schemes和table
	 * @param schemes
	 * @return
	 */
	List<Schemes> searchListAndTable(Schemes schemes);
	
}
