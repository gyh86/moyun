package com.gyh.system.gen.dao;

import java.util.List;

import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;
import com.gyh.system.gen.entity.Tables;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月20日 下午11:20:35
* 
*/
@MyBatisDao
public interface TablesDao extends BaseDao<Tables>{

	/**
	 * 获取表中数据以及表中的字段
	 * @param entity 表的实体类
	 * @return 
	 */
	Tables searchEntity(Tables entity);
	
	/**
	 * 获取本数据库中所有的表
	 * @return
	 */
	List<Tables> searchTables();
	
	
	
}
