package com.gyh.system.gen.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;
import com.gyh.system.gen.entity.Columns;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月22日 下午8:18:12
* 
*/
@MyBatisDao
public interface ColumnsDao extends BaseDao<Columns>{

	/**
	 * 根据表名获取表中所有的字段属性
	 * @param table
	 * @return
	 */
	List<Columns> searchColumns(@Param("table")String table);
}
