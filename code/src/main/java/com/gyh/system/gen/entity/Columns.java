package com.gyh.system.gen.entity;

import com.gyh.system.common.persistence.BaseEntity;
import com.gyh.system.common.tools.StringUtils;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月22日 下午7:58:20
* 
*/
public class Columns extends BaseEntity<Columns>{

	private static final long serialVersionUID = 1L;
	
	private Long tId;              //所属表的ID
	private String jdbcName;		//数据库中字段名
	private String javaName;		//java属性名
	private String comments;		//数据库中的注释
	private String jdbcType;		//数据库中的字段类型
	private String javaType;		//java中的属性类型
	private String isPk;			//是否是主键
	private String isNull;			//是否为空
	private String isInsert;	// 是否为插入字段（1：插入字段）
	private String isQuery;			//是否用于查询条件
	private String isEdit;		// 是否编辑字段（1：编辑字段）
	private String queryType;		//查询类型
	private Integer length;			//字段的长度
	private String dictType;	// 字典类型
	private Integer sort;		// 排序（升序）
	
	
	
	public Long gettId() {
		return tId;
	}
	public void settId(Long tId) {
		this.tId = tId;
	}
	public String getJdbcName() {
		return jdbcName;
	}
	public void setJdbcName(String jdbcName) {
		this.jdbcName = jdbcName;
	}
	public String getJavaName() {
		return javaName;
	}
	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getJdbcType() {
		return jdbcType;
	}
	public void setJdbcType(String jdbcType) {
		this.jdbcType = jdbcType;
	}
	public String getJavaType() {
		return javaType;
	}
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	public String getIsPk() {
		return isPk;
	}
	public void setIsPk(String isPk) {
		this.isPk = isPk;
	}
	public String getIsNull() {
		return isNull;
	}
	public void setIsNull(String isNull) {
		this.isNull = isNull;
	}
	public String getIsInsert() {
		if(StringUtils.isBlank(isInsert) && !"id".equals(javaName)) {
			return "1";
		} else if (StringUtils.isBlank(isInsert) && "id".equals(javaName)) {
			return "0";
		}
		return isInsert;
	}
	public void setIsInsert(String isInsert) {
		this.isInsert = isInsert;
	}
	public String getIsQuery() {
		if(StringUtils.isBlank(isQuery) && !"id".equals(javaName) && !"del".equals(javaName)) {
			return "0";
		} else if (StringUtils.isBlank(isQuery) && ("id".equals(javaName) || "del".equals(javaName))) {
			return "1";
		}
		return isQuery;
	}
	public void setIsQuery(String isQuery) {
		this.isQuery = isQuery;
	}
	public String getIsEdit() {
		if(StringUtils.isBlank(isEdit) && !"createBy".equals(javaName) && !"createTime".equals(javaName) 
				 && !"del".equals(javaName) && !"id".equals(javaName)) {
			return "1";
		} else if(StringUtils.isBlank(isEdit) || "createBy".equals(javaName) || "createTime".equals(javaName) 
				 || "del".equals(javaName) || "id".equals(javaName)) {
			return "0";
		}
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	public String getQueryType() {
		if (StringUtils.isBlank(isQuery) && ("id".equals(javaName) || "del".equals(javaName))) {
			return "=";
		}
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public String getDictType() {
		return dictType;
	}
	public void setDictType(String dictType) {
		this.dictType = dictType;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	


}
