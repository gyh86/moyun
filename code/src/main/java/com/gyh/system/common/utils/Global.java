package com.gyh.system.common.utils;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;

/**
 * 
 * @author 作者 gyh
 * @version 创建时间：2018年7月2日 下午3:33:42
 */
public class Global {
	
	/**
	 * Token的过期时间(分钟)
	 */
	public static final int TOKEN_EXPIRE = 12 * 3600; 
	
	/**
	 * 获取的项目工程路径
	 */
	public static final String PATH_PROJECT = System.getProperty("user.dir");
	
	/**
	 * 保存全局属性值
	 */
	private static Map<String, String> map = Maps.newHashMap();
	
	/**
	 * 获取配置文件中的数据
	 */
	public static String getConfig(String key) {
		String value = map.get(key);
		if (StringUtils.isEmpty(value)) {
			value = SysConfigProperties.getValue(key);
			map.put(key, StringUtils.isEmpty(value) ? value : StringUtils.EMPTY);
		}
		return value;
	}
	
	/**
	 * 判断是否是debug模式
	 */
	public static Boolean isDebugMode() {
		String dm = getConfig("debug.mode");
		return "true".equals(dm) || "1".equals(dm);
	}
	
	
}
