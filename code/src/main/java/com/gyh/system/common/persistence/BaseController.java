package com.gyh.system.common.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
* @author 作者 gyh:
* @version 创建时间：2018年1月6日 下午11:39:15
* 
*/
public abstract class BaseController {
	protected Logger log = LoggerFactory.getLogger(getClass());
	
//	/**
//	 * 管理基础路径
//	 */
//	@Value("${adminPath}")
//	protected String adminPath;
//	
//	/**
//	 * 前端基础路径
//	 */
//	@Value("${frontPath}")
//	protected String frontPath;
//	
//	/**
//	 * 前端URL后缀
//	 */
//	@Value("${urlSuffix}")
//	protected String urlSuffix;
	
}
