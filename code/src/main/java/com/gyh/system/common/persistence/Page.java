package com.gyh.system.common.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gyh.system.common.tools.StringUtils;
import com.gyh.system.common.utils.CookieUtils;
import com.gyh.system.common.utils.Global;

/**
 * 用于分页类
 * @author 作者 gyh
 * @version 创建时间：2018年7月7日 下午4:55:05
 */
public class Page<T> {

	private int pageNo = 1; // 当前页码
	private int pageSize = Integer.valueOf(Global.getConfig("page.pageSize")); // 页面大小，设置为“-1”表示不进行分页（分页无效）
	private long total;// 总记录数，设置为“-1”表示不查询总数
	private int first;// 首页索引
	private int last;// 尾页索引
	// private int prev;// 上一页索引
	// private int next;// 下一页索引
	private boolean firstPage;// 是否是第一页
	private boolean lastPage;// 是否是最后一页
	// private int length = 8;// 显示页面长度
	// private int slider = 1;// 前后显示页面长度
	private List<T> list = new ArrayList<T>();
	private Map<String, Object> params = new HashMap<String, Object>();// 参数我们把它分装成一个Map对象
	private String orderBy = ""; // 标准查询有效， 实例： updatedate desc, name asc
	private String funcName = "page"; // 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。
	private String funcParam = ""; // 函数的附加参数，第三个参数值。
	// private String message = ""; // 设置提示消息，显示在“共n条”之后
	
	public Page() {
		this.pageSize = -1;
	}
	
	/**
	 * 构造方法
	 */
	public Page(int pageNo,int pageSize) {
		this(pageNo, pageSize, 0);
	}
	
	
	
	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 * @param list
	 *            本页数据对象列表
	 */
	public Page(int pageNo, int pageSize, long count) {
		this(pageNo, pageSize, count, new ArrayList<>());
	}
	
	/**
	 * 构造方法
	 * 
	 * @param pageNo
	 *            当前页码
	 * @param pageSize
	 *            分页大小
	 * @param count
	 *            数据条数
	 * @param list
	 *            本页数据对象列表
	 */
	public Page(int pageNo, int pageSize, long count, List<T> list) {
		this.setTotal(total);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.list = list;
	}
	
	/**
	 * 构造方法
	 * 
	 * @param request
	 *            传递 repage 参数，来记住页码
	 * @param response
	 *            用于设置 Cookie，记住页码
	 * @param defaultPageSize
	 *            默认分页大小，如果传递 -1 则为不分页，返回所有数据
	 */
	public Page(HttpServletRequest request, HttpServletResponse response, int defaultPageSize) {
		//设置页码参数（传递repage参数）
		String no = request.getParameter("pageNo");
		if (StringUtils.isNumber(no)) {
			CookieUtils.setCookie(response, "pageNo", no);
			this.pageNo = Integer.parseInt(no);
		} else if (request.getParameter("repage") != null) {
			no = CookieUtils.getCookie(request, "pageNo");
			if (StringUtils.isNumber(no)) {
				this.setPageNo(Integer.parseInt(no));
			}
		}
		//设置页面分页函数
		String funcName = request.getParameter("funcName");
		if (StringUtils.isNotBlank(funcName)) {
			CookieUtils.setCookie(response, "funcName", funcName);
			this.setFuncName(funcName);
		} else if (request.getParameter("repage") != null) {
			funcName = CookieUtils.getCookie(request, "funcName");
			if (StringUtils.isNotBlank(funcName)) {
				this.setFuncName(funcName);
			}
		}
		//设置排序参数
		String orderBy = request.getParameter("orderBy");
		if (StringUtils.isNotBlank(orderBy)) {
			this.setOrderBy(orderBy);
		}
	}
	
	
	public void initialze() {
		this.first = 1;
		
		this.last = (int) (this.total/(this.pageSize < 1 ? 20 : this.pageSize) + first -1);
		
		if (this.total % this.pageSize != 0 || this.last == 0) {
			this.last++;
		}

		if (this.last < this.first) {
			this.last = this.first;
		}

		if (this.pageNo <= 1) {
			this.pageNo = this.first;
			this.firstPage = true;
		}

		if (this.pageNo >= this.last) {
			this.pageNo = this.last;
			this.lastPage = true;
		}

		// if (this.pageNo < this.last - 1) {
		// this.next = this.pageNo + 1;
		// } else {
		// this.next = this.last;
		// }
		//
		// if (this.pageNo > 1) {
		// this.prev = this.pageNo - 1;
		// } else {
		// this.prev = this.first;
		// }

		// 2
		if (this.pageNo < this.first) {// 如果当前页小于首页
			this.pageNo = this.first;
		}

		if (this.pageNo > this.last) {// 如果当前页大于尾页
			this.pageNo = this.last;
		}
	}

	
	public Map<String,Object> getParams(){
		return params;
	}
	
	public Page<T> setParams(Map<String, Object> params) {
		this.params = params;
		return this;
	}
	
	/**
	 * 获取设置总数
	 * 
	 * @return
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * 设置数据总数
	 * 
	 * @param count
	 */
	public void setTotal(long total) {
		this.total = total;
		if (pageSize >= total) {
			pageNo = 1;
		}
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * 设置当前页码
	 * 
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 获取页面大小
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小（最大500）
	 * 
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;// > 500 ? 500 : pageSize;
	}

	/**
	 * 首页索引
	 * 
	 * @return
	 */
	@JsonIgnore
	public int getFirst() {
		return first;
	}

	/**
	 * 尾页索引
	 * 
	 * @return
	 */
	@JsonIgnore
	public int getLast() {
		return last;
	}

	/**
	 * 获取页面总数
	 * 
	 * @return getLast();
	 */
	@JsonIgnore
	public int getTotalPage() {
		return getLast();
	}

	/**
	 * 是否为第一页
	 * 
	 * @return
	 */
	@JsonIgnore
	public boolean isFirstPage() {
		return firstPage;
	}

	/**
	 * 是否为最后一页
	 * 
	 * @return
	 */
	@JsonIgnore
	public boolean isLastPage() {
		return lastPage;
	}

	/**
	 * 上一页索引值
	 * 
	 * @return
	 */
	@JsonIgnore
	public int getPrev() {
		if (isFirstPage()) {
			return pageNo;
		} else {
			return pageNo - 1;
		}
	}

	/**
	 * 下一页索引值
	 * 
	 * @return
	 */
	@JsonIgnore
	public int getNext() {
		if (isLastPage()) {
			return pageNo;
		} else {
			return pageNo + 1;
		}
	}
	
	
	/**
	 * 获得本页面数据对象集合
	 */
	public List<T> getList(){
		return list;
	}
	
	/**
	 * 设置本页数据对象列表
	 */
	public Page<T> setList(List<T> list){
		this.list = list;
		initialze();
		return this;
	}
	
	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	@JsonIgnore
	public String getOrderBy() {
		String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
				+ "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";
		Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
		if (sqlPattern.matcher(orderBy).find()) {
			return "";
		}
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	/**
	 * 获取首行 FirstResult
	 */
	public int getFirstResult() {
		int firstResult = (getPageNo() - 1) * getPageSize();
		if (firstResult >= getTotal()) {
			firstResult = 0;
		}
		return firstResult;
	}

	/**
	 * 获取 Hibernate MaxResults
	 */
	public int getMaxResults() {
		return getPageSize();
	}
	
	
	
}
