package com.gyh.system.common.exception;

/**
 * 
 * @author 作者 gyh
 * @version 创建时间：2018年7月3日 下午11:07:45
 */
public class RException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private int code = 500;
	
	private String msg;
	
	public RException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public RException(String msg,Throwable e) {
		super(msg,e);
		this.msg = msg;
	}

	public RException(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public RException(int code, String msg,Throwable e) {
		super(msg,e);
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
