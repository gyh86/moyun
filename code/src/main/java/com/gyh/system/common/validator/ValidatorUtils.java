package com.gyh.system.common.validator;

import com.gyh.system.common.exception.RException;
import com.gyh.system.common.tools.StringUtils;

/**
 * 
 * @author 作者 gyh
 * @version 创建时间：2018年7月11日 下午11:23:37
 */
public class ValidatorUtils {
	
	/**
	 * 校验一定不能为空的字符串
	 * @param str 校验数据
	 * @param message 提示信息
	 */
	public static void isBlank(String str, String message) {
		if (StringUtils.isBlank(str)) {
			throw new RException(message);
		}
	}

}
