package com.gyh.system.common.persistence;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * 创建实体类的根源类
 * @author gyh
 * @param <T>
 */
public abstract class RootEntity<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	protected Long id;			//编号
	
	protected Page<T> page;
	
	
	public RootEntity() {
		
	}
	
	public RootEntity(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@XmlTransient //防止数据库中的字段和属性的映射
	public Page<T> getPage() {
		if (page == null) {
			page = new Page<T>();
		}
		return page;
	}

	public Page<T> setPage(Page<T> page) {
		this.page = page;
		return page;
	}

	/**
	 * 插入之前执行
	 */
	public abstract void preInsert();
	
	/**
	 * 修改执行执行
	 */
	public abstract void preUpdate();
	
	/**
	 * 删除标记（0：正常；1：删除；2：审核；）
	 */
	public static final String DEL_Y = "Y";
	public static final String DEL_N = "N";

}
