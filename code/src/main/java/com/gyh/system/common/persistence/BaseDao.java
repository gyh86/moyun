package com.gyh.system.common.persistence;

import java.util.List;
/**
 * 创建Dao的基础类
 * @author gyh
 * @param <T>
 */
public interface BaseDao<T>{
	
	/**
	 * 根据ID获取单条数据
	 * @param id
	 * @return
	 */
	public T get(Long id);
	
	/**
	 * 根据实体类获取单条数据
	 * @param entity
	 * @return
	 */
	//public T get(T entity);
	
	/**
	 * 根据实体类 获取数据集合
	 * @param entity
	 * @return
	 */
	public List<T> findList(T entity);
	
	/**
	 * 获取数据集合
	 * @param entity
	 * @return
	 */
	public List<T> findAllList();
	
	/**
	 * 插入数据
	 * @param entity
	 * @return
	 */
	public Integer insert(T entity);
	
	/**
	 * 批量插入数据
	 * @param entityList
	 * @return
	 */
	public Integer insertList(List<T> entityList);
	
	/**
	 * 更新单条数据
	 * @param entity
	 * @return
	 */
	public Integer update(T entity);
	
	/**
	 * 批量更新数据
	 * @param entityList
	 * @return
	 */
	//public Integer updateList(List<T> entityList);
	
	/**
	 * 根据实体类 删除数据
	 * @param entity
	 * @return
	 */
	public Integer delete(T entity);
	
	/**
	 * 根据ID 删除数据
	 * @param id
	 * @return
	 */
	public Integer delete(Long id);
	
	/**
	 * 根据ID 执行物理删除数据
	 * @param id
	 * @return
	 */
	public Integer deleteAlways(Long id);
	
	/**
	 * 批量保存，如果有ID执行修改，否者执行添加
	 * 在执行批量操作时使用了多条SQL语句（执行多条SQL语句，必须在jdbc.url的参数后面添加 allowMultiQueries=true）
	 * 让mysql允许执行 多条sql语句
	 * @param list
	 * @return
	 */
	//public Integer saveList(List<T> list);
	
}
