package com.gyh.system.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.gyh.system.common.utils.R;

/**
 * 异常处理
 * @author 作者 gyh
 * @version 创建时间：2018年7月7日 上午1:03:20
 */
@RestControllerAdvice
public class RExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 自定义异常
	 */
	@ExceptionHandler(RException.class)
	public R handlerRException(RException e) {
		return R.error(e.getCode(), e.getMessage());
	}
	
	/**
	 * 数据的唯一冲突
	 */
	@ExceptionHandler(DuplicateKeyException.class)
	public R handleDuplicateKeyException(DuplicateKeyException e){
		logger.error(e.getMessage(), e);
		return R.error("数据库中已存在该记录");
	}
	
	@ExceptionHandler(Exception.class)
	public R handleException(Exception e){
		logger.error(e.getMessage(), e);
		return R.error();
	}
	

}
