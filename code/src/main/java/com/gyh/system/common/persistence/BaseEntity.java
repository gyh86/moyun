package com.gyh.system.common.persistence;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gyh.system.sys.entity.User;

/**
 * 创建实体类的基础类
 * @author gyh
 * @param <T>
 */
public abstract class BaseEntity<T> extends RootEntity<T>{
	private static final long serialVersionUID = 1L;

	protected String remarks;   //备注信息
	protected User createBy;  //创建者
	protected Date createTime;  //创建时间
	protected User updateBy;  //修改者
	protected Date updateTime;  //修改时间
	protected String del = "N";		//删除标记（N：正常；Y：删除；）
	
	public BaseEntity() {
		
	}
	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@JsonIgnore
	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@JsonIgnore
	public User getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(User updateBy) {
		this.updateBy = updateBy;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDel() {
		if (StringUtils.isBlank(del)) return "N";
		return del;
	}

	public void setDel(String del) {
		this.del = del;
	}

	public BaseEntity(Long id) {
		super(id);
	}
	
	@Override
	public void preInsert() {
		// TODO Auto-generated method stub
		this.updateTime = new Date();
		this.createTime = this.updateTime;
	}

	@Override
	public void preUpdate() {
		// TODO Auto-generated method stub
		this.updateTime = new Date();
	}
	

}
