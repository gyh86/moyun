package com.gyh.system.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cookie工具
 * @author 作者 gyh
 * @version 创建时间：2018年7月7日 下午10:21:20
 */
public class CookieUtils {

	private static final Logger logger = LoggerFactory.getLogger(CookieUtils.class);
	
	/**
	 * 设置cookie（生成段时间一天）
	 * @param name 名称
	 * @param value 值
	 * @param i 
	 */
	public static void setCookie(HttpServletResponse response,String name,String value) {
		setCookie(response,name,value,60*60*24);
	}
	
	
	/**
	 * 设置cookie
	 * @param name 名称
	 * @param value 值
	 * @param path 路径
	 */
	public static void setCookie(HttpServletResponse response,String name,String value, String path) {
		setCookie(response,name,value,path,60*60*24);
	}
	
	/**
	 * 设置cookie
	 * @param name 名称
	 * @param value 值
	 * @param maxAge 生存时间（单位秒）
	 */
	public static void setCookie(HttpServletResponse response,String name,String value, int maxAge) {
		setCookie(response,name,value,"/",maxAge);
	}
	
	/**
	 * 设置cookie
	 * @param name 名称
	 * @param value 值
	 * @param path 路径
	 * @param maxAge 生存时间（单位秒）
	 */
	public static void setCookie(HttpServletResponse response,String name,String value,String path,int maxAge) {
		Cookie cookie = new Cookie(name,null);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		try {
			cookie.setValue(URLEncoder.encode(value, "utf-8"));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		response.addCookie(cookie);
	}
	
	/**
	 * 获取指定的Cookie值
	 * @param request
	 * @param name
	 * @return
	 */
	public static String getCookie(HttpServletRequest request,String name) {
		return getCookie(request,null,name,false);
	}
	
	/**
	 * 获取cookie值，并执行删除
	 * @param name 名字
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		return getCookie(request,response,name,true);
	}
	
	/**
	 * 获取指定Cookie的值
	 * @param request 请求对象 
	 * @param response 响应对象
	 * @param name 名字
	 * @param isRemove 是否移除
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request,HttpServletResponse response,String name,boolean isRemove) {
		String value = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			Optional<Cookie> opt = Arrays.asList(cookies).stream().filter(i->name.equals(i.getName())).findAny();
			if (opt.isPresent()) {
				try {
					value = URLDecoder.decode(opt.get().getValue(), "utf-8");
				} catch (UnsupportedEncodingException e) {
					logger.error(e.getMessage());
				}
				if (isRemove) {
					opt.get().setMaxAge(0);
					response.addCookie(opt.get());
				}
			}
		}
		return value;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
