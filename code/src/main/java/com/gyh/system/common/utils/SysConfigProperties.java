package com.gyh.system.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 系统中的配置文件
 * @author 作者 gyh
 * @version 创建时间：2018年7月7日 下午5:20:33
 */
public class SysConfigProperties {
	
	private static final Logger logger = LoggerFactory.getLogger(SysConfigProperties.class);
	
	private static final String SYS_CONFIG_NAME = "sys.config.name";
	
	public static Properties sysConfigProp;
	
	private static String sysConfigFileName;
	
	static {
		String path=SysConfigProperties.class.getClassLoader().getResource("/").getPath();
		logger.info("path============>"+path);
		Properties prop = System.getProperties();
		Object fileName = prop.get(SYS_CONFIG_NAME);
		
		if(fileName != null && StringUtils.isNotEmpty(String.valueOf(fileName)) ){
			sysConfigFileName = String.valueOf(fileName);
		}else{
			logger.error("系统参数配置文件不存在,请配置 VM arguments -Dsys.config.name");
		}
		InputStream is = null;
		try {
			is = SysConfigProperties.class.getClassLoader().getResourceAsStream(sysConfigFileName);
			sysConfigProp = new Properties();
			sysConfigProp.load(is);
			logger.info("Loading "+sysConfigFileName+" success");
		}
		catch (IOException ex) {
			logger.error("Could not load '"+sysConfigFileName+"'"+ex);
		}
		finally {
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 获取属性值
	 * @param key
	 * @return
	 */
	public static String getValue(String key) {
		return sysConfigProp.getProperty(key);
	}
	
	
	
	
	
	

}
