package com.gyh.system.common.persistence;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


/**
* @author 作者 gyh:
* @version 创建时间：2018年1月6日 上午12:20:17
* 这是基础的service类
*/
@Transactional(readOnly = true)
public abstract class BaseService<D extends BaseDao<T>, T extends BaseEntity<T>> {

	@Autowired
	private D dao;
	
	/**
	 * 根据ID获取单条数据
	 * @param id
	 * @return
	 */
	public T get(Long id) {
		return dao.get(id);
	}
	
	/**
	 * 根据实体类获取单条数据
	 * @param entity
	 * @return
	 */
	public T get(T entity){
		List<T> list = dao.findList(entity);
		if (list.isEmpty()) return null;
		return list.get(0);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param entity
	 * @return
	 */
	public Page<T> findPage(Page<T> page, T entity) {
		entity.setPage(page);
		page.setList(dao.findList(entity));
		return page;
	}

	
	/**
	 * 根据实体类 获取数据集合
	 * @param entity
	 * @return
	 */
	public List<T> findList(T entity){
		return dao.findList(entity);
	}
	
	/**
	 * 获取数据集合
	 * @param entity
	 * @return
	 */
	public List<T> findAllList(){
		return dao.findAllList();
	}
	
	/**
	 * 插入数据
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer insert(T entity){
		return dao.insert(entity);
	}
	
	/**
	 * 更新单条数据
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer update(T entity){
		return dao.update(entity);
	}
	
	/**
	 * 保存数据，如果有ID就执行修改，否者执行添加
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer save(T entity) {
		int num = 0;
		if (entity.getId() == null) {
			entity.preInsert();
			num = dao.insert(entity);
		} else {
			entity.preUpdate();
			num = dao.update(entity);
		}
		return num;
	}
	
	/**
	 * 根据实体类 删除数据
	 * @param entity
	 * @return
	 */
//	@Transactional(readOnly = false)
//	public Integer delete(T entity){
//		return dao.delete(entity);
//	}
	
	/**
	 * 根据ID 删除数据
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer delete(Long id){
		return dao.delete(id);
	}
	
	/**
	 * 根据ID 执行物理删除数据
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer deleteAlways(Long id) {
		return dao.deleteAlways(id);
	}
	
	/*------------------------------------ 批量处理数据 -----------------------------------*/
	
	/**
	 * 批量插入数据
	 * @param entityList
	 * @return
	 */
	@Transactional(readOnly = false)
	public Integer insertList(List<T> entityList){
		return dao.insertList(entityList);
	}
	
	/**
	 * 批量更新数据
	 * @param entityList
	 * @return
	 */
//	@Transactional(readOnly = false)
//	public int updateList(List<T> entityList){
//		return dao.updateList(entityList);
//	}
	
	/**
	 * 批量保存，如果有ID执行修改，否者执行添加
	 * @param list
	 * @return
	 */
//	@Transactional(readOnly = false)
//	public int saveList(List<T> list) {
//		return dao.saveList(list);
//	}
	
	
	
}
