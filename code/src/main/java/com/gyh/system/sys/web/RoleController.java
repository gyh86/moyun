package com.gyh.system.sys.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.sys.entity.Role;
import com.gyh.system.sys.service.RoleService;
import com.gyh.system.common.utils.R;
import com.gyh.system.common.persistence.Page;
import com.gyh.system.common.persistence.BaseController;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统角色
*/
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController{
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * 查询数据
	 * @return
	 */
	@GetMapping
	public R getList(Role role) {
		List<Role> roles = roleService.findList(role);
		return R.ok(roles);
	}
	
	/**
	 * 分页查询数据
	 * @return
	 */
	@GetMapping("page")
	public R getList(Role role,@RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
		Page<Role> page = roleService.findPage(new Page<Role>(pageNo,pageSize), role);
		return R.ok(page);
	}
	
	/**
	 * 查询表中全部数据
	 * @return
	 */
	@GetMapping("getAll")
	public R getALLList(Role role) {
		List<Role> roles = roleService.findList(role);
		return R.ok(roles);
	}
	
	/**
	 * 保存单条数据
	 * @return
	 */
	@PostMapping
	public R save(@RequestBody Role role) {
		roleService.save(role);
		return R.ok("保存成功");
	}
	
	/**
	 * 批量保存数据
	 * @return
	 */
	@PostMapping("save")
	public R saves(@RequestBody List<Role> list) {
		for (Role role : list) {
			roleService.save(role);
		}
		return R.ok("保存成功");
	}
	
	/**
	 * 根据ID执行逻辑删除
	 * @return
	 */
	@GetMapping("delete")
	public R delete(Long id) {
		int rows = roleService.delete(id);
		if (rows > 0) {
			return R.ok("删除成功");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
	/**
	 * 根据ID执行物理删除
	 * @return
	 */
	@GetMapping("remove")
	public R deleteAlways(Long id) {
		int rows = roleService.deleteAlways(id);
		if (rows > 0) {
			return R.ok("已永久删除");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
}