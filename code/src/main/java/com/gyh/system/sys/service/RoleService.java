package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.RoleDao;
import com.gyh.system.sys.entity.Role;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统角色
*/
@Service
public class RoleService extends BaseService<RoleDao, Role> {

}