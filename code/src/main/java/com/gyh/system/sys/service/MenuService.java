package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.MenuDao;
import com.gyh.system.sys.entity.Menu;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统菜单
*/
@Service
public class MenuService extends BaseService<MenuDao, Menu> {

}