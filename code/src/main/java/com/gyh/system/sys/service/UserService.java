package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.UserDao;
import com.gyh.system.sys.entity.User;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统用户
*/
@Service
public class UserService extends BaseService<UserDao, User> {

}