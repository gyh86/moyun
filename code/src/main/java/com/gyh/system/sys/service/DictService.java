package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.DictDao;
import com.gyh.system.sys.entity.Dict;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统字典
*/
@Service
public class DictService extends BaseService<DictDao, Dict> {

}