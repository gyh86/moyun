package com.gyh.system.sys.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.sys.entity.Dict;
import com.gyh.system.sys.service.DictService;
import com.gyh.system.common.utils.R;
import com.gyh.system.common.persistence.Page;
import com.gyh.system.common.persistence.BaseController;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统字典
*/
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController{
	
	@Autowired
	private DictService dictService;
	
	/**
	 * 查询数据
	 * @return
	 */
	@GetMapping
	public R getList(Dict dict) {
		List<Dict> dicts = dictService.findList(dict);
		return R.ok(dicts);
	}
	
	/**
	 * 分页查询数据
	 * @return
	 */
	@GetMapping("page")
	public R getList(Dict dict,@RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
		Page<Dict> page = dictService.findPage(new Page<Dict>(pageNo,pageSize), dict);
		return R.ok(page);
	}
	
	/**
	 * 查询表中全部数据
	 * @return
	 */
	@GetMapping("getAll")
	public R getALLList(Dict dict) {
		List<Dict> dicts = dictService.findList(dict);
		return R.ok(dicts);
	}
	
	/**
	 * 保存单条数据
	 * @return
	 */
	@PostMapping
	public R save(@RequestBody Dict dict) {
		dictService.save(dict);
		return R.ok("保存成功");
	}
	
	/**
	 * 批量保存数据
	 * @return
	 */
	@PostMapping("save")
	public R saves(@RequestBody List<Dict> list) {
		for (Dict dict : list) {
			dictService.save(dict);
		}
		return R.ok("保存成功");
	}
	
	/**
	 * 根据ID执行逻辑删除
	 * @return
	 */
	@GetMapping("delete")
	public R delete(Long id) {
		int rows = dictService.delete(id);
		if (rows > 0) {
			return R.ok("删除成功");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
	/**
	 * 根据ID执行物理删除
	 * @return
	 */
	@GetMapping("remove")
	public R deleteAlways(Long id) {
		int rows = dictService.deleteAlways(id);
		if (rows > 0) {
			return R.ok("已永久删除");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
}