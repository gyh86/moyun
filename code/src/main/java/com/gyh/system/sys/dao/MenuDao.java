package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.Menu;

import java.util.List;

import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统菜单
*/
@MyBatisDao
public interface MenuDao extends BaseDao<Menu>{

	/**
	 * 根据用户ID获取权限列表
	 * @param menu
	 * @return
	 */
	List<Menu> findByUserId(Menu menu);

}