package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.User;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统用户
*/
@MyBatisDao
public interface UserDao extends BaseDao<User>{

	/**
	 * 根据登入名获取用户信息
	 * @param username
	 * @return
	 */
	User getUserByUsername(String username);
	
	
	
	
}