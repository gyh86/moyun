package com.gyh.system.sys.utils;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * Token生成器
 * @author 作者 gyh
 * @version 创建时间：2018年7月11日 下午10:05:43
 */
public class TokenGenerator {

	public static String generatorToken() {
		 return StringUtils.replace(UUID.randomUUID().toString(), "-", "");
	}
	
	
	
}
