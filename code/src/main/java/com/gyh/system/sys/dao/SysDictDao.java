package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.SysDict;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试数据
*/
@MyBatisDao
public interface SysDictDao extends BaseDao<SysDict>{

}