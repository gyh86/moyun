package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.Office;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统机构表
*/
@MyBatisDao
public interface OfficeDao extends BaseDao<Office>{

}