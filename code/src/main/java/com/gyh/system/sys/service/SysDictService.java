package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.SysDictDao;
import com.gyh.system.sys.entity.SysDict;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试数据
*/
@Service
public class SysDictService extends BaseService<SysDictDao, SysDict> {

}