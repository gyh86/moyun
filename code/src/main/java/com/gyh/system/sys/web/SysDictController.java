package com.gyh.system.sys.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.system.sys.entity.SysDict;
import com.gyh.system.sys.service.SysDictService;

import ch.qos.logback.classic.Logger;

import com.gyh.system.common.utils.Global;
import com.gyh.system.common.utils.R;
import com.gyh.system.common.persistence.BaseController;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试数据
*/
@RestController
@RequestMapping("/sysDict")
public class SysDictController extends BaseController{
	
	@Autowired
	private SysDictService sysDictService;
	
	/**
	 * 查询数据
	 * @return
	 */
	@GetMapping
	public R getList(SysDict sysDict) {
		
		String str = Global.getConfig("jdbc.password");
		log.info("config===>"+str);
		List<SysDict> sysDicts = sysDictService.findList(sysDict);
		return R.ok(sysDicts);
	}
	
	/**
	 * 查询表中全部数据
	 * @return
	 */
	@GetMapping("getAll")
	public R getALLList(SysDict sysDict) {
		List<SysDict> sysDicts = sysDictService.findList(sysDict);
		return R.ok(sysDicts);
	}
	
	/**
	 * 保存单条数据
	 * @return
	 */
	@PostMapping
	public R save(@RequestBody SysDict sysDict) {
		sysDictService.save(sysDict);
		return R.ok("保存成功");
	}
	
	/**
	 * 批量保存数据
	 * @return
	 */
	@PostMapping("save")
	public R saves(@RequestBody List<SysDict> list) {
		for (SysDict sysDict : list) {
			sysDictService.save(sysDict);
		}
		return R.ok("保存成功");
	}
	
	/**
	 * 根据ID执行逻辑删除
	 * @return
	 */
	@GetMapping("delete")
	public R delete(Long id) {
		int rows = sysDictService.delete(id);
		if (rows > 0) {
			return R.ok("删除成功");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
	/**
	 * 根据ID执行物理删除
	 * @return
	 */
	@GetMapping("remove")
	public R deleteAlways(Long id) {
		int rows = sysDictService.deleteAlways(id);
		if (rows > 0) {
			return R.ok("已永久删除");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
}