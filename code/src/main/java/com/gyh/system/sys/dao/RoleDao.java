package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.Role;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统角色
*/
@MyBatisDao
public interface RoleDao extends BaseDao<Role>{

}