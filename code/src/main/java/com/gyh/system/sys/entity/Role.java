package com.gyh.system.sys.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统角色
*/
public class Role extends BaseEntity<Role>{

    private static final long serialVersionUID = 1L;

    private Long officeId;		//归属机构
    private String name;		//角色名称
    private String enname;		//英文名称
    private String roleType;		//角色类型
    private String dataScope;		//数据范围
    private String isSys;		//是否系统数据
    private String useable;		//是否可用
  
    private User user;		// 根据用户ID查询角色列表
    
    
    public Role() {
		super();
	}
    
    public Role(User user) {
		super();
		this.user = user;
	}
      
    public Long getOfficeId() {
        return officeId;
    }
  
    public void setOfficeId(Long officeId){
        this.officeId = officeId;
    }
    
	public String getName() {
        return name;
    }
  
    public void setName(String name){
        this.name = name;
    }
      
    public String getEnname() {
        return enname;
    }
  
    public void setEnname(String enname){
        this.enname = enname;
    }
      
    public String getRoleType() {
        return roleType;
    }
  
    public void setRoleType(String roleType){
        this.roleType = roleType;
    }
      
    public String getDataScope() {
        return dataScope;
    }
  
    public void setDataScope(String dataScope){
        this.dataScope = dataScope;
    }
      
    public String getIsSys() {
        return isSys;
    }
  
    public void setIsSys(String isSys){
        this.isSys = isSys;
    }
      
    public String getUseable() {
        return useable;
    }
  
    public void setUseable(String useable){
        this.useable = useable;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
  
}
