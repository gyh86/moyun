package com.gyh.system.sys.utils;

/**
 * 
 * @author 作者 gyh
 * @version 创建时间：2018年7月11日 下午8:56:20
 */
public class UserModel {

	private String username; 
	private String password; 
	private String captcha;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCaptcha() {
		return captcha;
	}
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
	
}
