package com.gyh.system.sys.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统用户
*/
public class User extends BaseEntity<User>{

    private static final long serialVersionUID = 1L;

    private Long companyId;		//归属公司
    private Long officeId;		//归属部门
    private String loginName;		//登录名
    private String password;		//密码
    private String no;		//工号
    private String name;		//姓名
    private String email;		//邮箱
    private String phone;		//电话
    private String mobile;		//手机
    private String userType;		//用户类型
    private String photo;		//用户头像
    private String loginIp;		//最后登陆IP
    private Date loginDate;		//最后登陆时间
    private String loginFlag;		//是否可登录
  
    private List<Role> roleList = Lists.newArrayList(); // 拥有角色列表
      
    public Long getCompanyId() {
        return companyId;
    }
  
    public void setCompanyId(Long companyId){
        this.companyId = companyId;
    }
      
    public Long getOfficeId() {
        return officeId;
    }
  
    public void setOfficeId(Long officeId){
        this.officeId = officeId;
    }
      
    public String getLoginName() {
        return loginName;
    }
  
    public void setLoginName(String loginName){
        this.loginName = loginName;
    }
      
    public String getPassword() {
        return password;
    }
  
    public void setPassword(String password){
        this.password = password;
    }
      
    public String getNo() {
        return no;
    }
  
    public void setNo(String no){
        this.no = no;
    }
      
    public String getName() {
        return name;
    }
  
    public void setName(String name){
        this.name = name;
    }
      
    public String getEmail() {
        return email;
    }
  
    public void setEmail(String email){
        this.email = email;
    }
      
    public String getPhone() {
        return phone;
    }
  
    public void setPhone(String phone){
        this.phone = phone;
    }
      
    public String getMobile() {
        return mobile;
    }
  
    public void setMobile(String mobile){
        this.mobile = mobile;
    }
      
    public String getUserType() {
        return userType;
    }
  
    public void setUserType(String userType){
        this.userType = userType;
    }
      
    public String getPhoto() {
        return photo;
    }
  
    public void setPhoto(String photo){
        this.photo = photo;
    }
      
    public String getLoginIp() {
        return loginIp;
    }
  
    public void setLoginIp(String loginIp){
        this.loginIp = loginIp;
    }
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    public Date getLoginDate() {
        return loginDate;
    }
  
    public void setLoginDate(Date loginDate){
        this.loginDate = loginDate;
    }
      
    public String getLoginFlag() {
        return loginFlag;
    }
  
    public void setLoginFlag(String loginFlag){
        this.loginFlag = loginFlag;
    }

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	
	/**
	 * 判断用户是否是管理员
	 * @return
	 */
	public boolean isAdmin(){
		return isAdmin(this.id);
	}
	
	public static boolean isAdmin(Long id){
		return id != null && 1==id;
	}
    
}
