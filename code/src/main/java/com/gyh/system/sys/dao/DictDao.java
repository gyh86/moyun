package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.Dict;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统字典
*/
@MyBatisDao
public interface DictDao extends BaseDao<Dict>{

}