package com.gyh.system.sys.entity;

import java.util.Date;

import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试数据
*/
public class SysDict extends BaseEntity<SysDict>{
  
  private static final long serialVersionUID = 1L;

      private String value;
      private String label;
      private String type;
      private String description;
      private Integer sort;
      private Long parentId;
                	
	
    	    	    
    public String getValue() {
      return value;
    }
    public void setValue(String value){
      this.value = value;
    }
        	    
    public String getLabel() {
      return label;
    }
    public void setLabel(String label){
      this.label = label;
    }
        	    
    public String getType() {
      return type;
    }
    public void setType(String type){
      this.type = type;
    }
        	    
    public String getDescription() {
      return description;
    }
    public void setDescription(String description){
      this.description = description;
    }
        	    
    public Integer getSort() {
      return sort;
    }
    public void setSort(Integer sort){
      this.sort = sort;
    }
        	    
    public Long getParentId() {
      return parentId;
    }
    public void setParentId(Long parentId){
      this.parentId = parentId;
    }
        	    	    	    	    	    	  
}
