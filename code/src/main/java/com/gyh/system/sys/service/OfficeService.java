package com.gyh.system.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.system.sys.dao.OfficeDao;
import com.gyh.system.sys.entity.Office;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统机构表
*/
@Service
public class OfficeService extends BaseService<OfficeDao, Office> {

}