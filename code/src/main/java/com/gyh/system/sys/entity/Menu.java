package com.gyh.system.sys.entity;

import java.util.List;

import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统菜单
*/
public class Menu extends BaseEntity<Menu>{

    private static final long serialVersionUID = 1L;

    private Long parentId;		//父级编号
    private String parentIds;		//所有父级编号
    private String name;		//名称
    private Integer sort;		//排序
    private String href;		//链接
    private String target;		//目标
    private String icon;		//图标
    private String isShow;		//是否在菜单中显示
    private String permission;		//权限标识
  
    private Long userId; //用户的ID编号
	private List<Menu> childList; // 子级菜单
	
	
      
    public Menu() {
		super();
	}

	public Long getParentId() {
        return parentId;
    }
  
    public void setParentId(Long parentId){
        this.parentId = parentId;
    }
      
    public String getParentIds() {
        return parentIds;
    }
  
    public void setParentIds(String parentIds){
        this.parentIds = parentIds;
    }
      
    public String getName() {
        return name;
    }
  
    public void setName(String name){
        this.name = name;
    }
      
    public Integer getSort() {
        return sort;
    }
  
    public void setSort(Integer sort){
        this.sort = sort;
    }
      
    public String getHref() {
        return href;
    }
  
    public void setHref(String href){
        this.href = href;
    }
      
    public String getTarget() {
        return target;
    }
  
    public void setTarget(String target){
        this.target = target;
    }
      
    public String getIcon() {
        return icon;
    }
  
    public void setIcon(String icon){
        this.icon = icon;
    }
      
    public String getIsShow() {
        return isShow;
    }
  
    public void setIsShow(String isShow){
        this.isShow = isShow;
    }
      
    public String getPermission() {
        return permission;
    }
  
    public void setPermission(String permission){
        this.permission = permission;
    }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Menu> getChildList() {
		return childList;
	}

	public void setChildList(List<Menu> childList) {
		this.childList = childList;
	}
    
    
  
}
