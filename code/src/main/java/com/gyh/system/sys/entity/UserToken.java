package com.gyh.system.sys.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统用户Token
*/
public class UserToken extends BaseEntity<UserToken>{

    private static final long serialVersionUID = 1L;

    private Long userId;		//用户的ID编号
    private String token;		//token
    private Date expireTime;		//过期时间
  
	
      
    public Long getUserId() {
        return userId;
    }
  
    public void setUserId(Long userId){
        this.userId = userId;
    }
      
    public String getToken() {
        return token;
    }
  
    public void setToken(String token){
        this.token = token;
    }
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    public Date getExpireTime() {
        return expireTime;
    }
  
    public void setExpireTime(Date expireTime){
        this.expireTime = expireTime;
    }
  
}
