package com.gyh.system.sys.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统字典
*/
public class Dict extends BaseEntity<Dict>{

    private static final long serialVersionUID = 1L;

    private String value;		//数据值
    private String label;		//标签名
    private String type;		//类型
    private String description;		//描述
    private Integer sort;		//排序（升序）
    private Long parentId;		//父级编号
  
	
      
    public String getValue() {
        return value;
    }
  
    public void setValue(String value){
        this.value = value;
    }
      
    public String getLabel() {
        return label;
    }
  
    public void setLabel(String label){
        this.label = label;
    }
      
    public String getType() {
        return type;
    }
  
    public void setType(String type){
        this.type = type;
    }
      
    public String getDescription() {
        return description;
    }
  
    public void setDescription(String description){
        this.description = description;
    }
      
    public Integer getSort() {
        return sort;
    }
  
    public void setSort(Integer sort){
        this.sort = sort;
    }
      
    public Long getParentId() {
        return parentId;
    }
  
    public void setParentId(Long parentId){
        this.parentId = parentId;
    }
  
}
