package com.gyh.system.sys.dao;

import com.gyh.system.sys.entity.UserToken;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统用户Token
*/
@MyBatisDao
public interface UserTokenDao extends BaseDao<UserToken>{

}