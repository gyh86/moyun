package com.gyh.system.sys.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 系统机构表
*/
public class Office extends BaseEntity<Office>{

    private static final long serialVersionUID = 1L;

    private Long parentId;		//父级编号
    private String parentIds;		//所有父级编号
    private String name;		//名称
    private Integer sort;		//排序
    private Long areaId;		//归属区域
    private String code;		//区域编码
    private String type;		//机构类型
    private String grade;		//机构等级
    private String address;		//联系地址
    private String zipCode;		//邮政编码
    private String master;		//负责人
    private String phone;		//电话
    private String fax;		//传真
    private String email;		//邮箱
    private String useable;		//是否启用
    private Long primaryPerson;		//主负责人
    private Long deputyPerson;		//副负责人
  
	
      
    public Long getParentId() {
        return parentId;
    }
  
    public void setParentId(Long parentId){
        this.parentId = parentId;
    }
      
    public String getParentIds() {
        return parentIds;
    }
  
    public void setParentIds(String parentIds){
        this.parentIds = parentIds;
    }
      
    public String getName() {
        return name;
    }
  
    public void setName(String name){
        this.name = name;
    }
      
    public Integer getSort() {
        return sort;
    }
  
    public void setSort(Integer sort){
        this.sort = sort;
    }
      
    public Long getAreaId() {
        return areaId;
    }
  
    public void setAreaId(Long areaId){
        this.areaId = areaId;
    }
      
    public String getCode() {
        return code;
    }
  
    public void setCode(String code){
        this.code = code;
    }
      
    public String getType() {
        return type;
    }
  
    public void setType(String type){
        this.type = type;
    }
      
    public String getGrade() {
        return grade;
    }
  
    public void setGrade(String grade){
        this.grade = grade;
    }
      
    public String getAddress() {
        return address;
    }
  
    public void setAddress(String address){
        this.address = address;
    }
      
    public String getZipCode() {
        return zipCode;
    }
  
    public void setZipCode(String zipCode){
        this.zipCode = zipCode;
    }
      
    public String getMaster() {
        return master;
    }
  
    public void setMaster(String master){
        this.master = master;
    }
      
    public String getPhone() {
        return phone;
    }
  
    public void setPhone(String phone){
        this.phone = phone;
    }
      
    public String getFax() {
        return fax;
    }
  
    public void setFax(String fax){
        this.fax = fax;
    }
      
    public String getEmail() {
        return email;
    }
  
    public void setEmail(String email){
        this.email = email;
    }
      
    public String getUseable() {
        return useable;
    }
  
    public void setUseable(String useable){
        this.useable = useable;
    }
      
    public Long getPrimaryPerson() {
        return primaryPerson;
    }
  
    public void setPrimaryPerson(Long primaryPerson){
        this.primaryPerson = primaryPerson;
    }
      
    public Long getDeputyPerson() {
        return deputyPerson;
    }
  
    public void setDeputyPerson(Long deputyPerson){
        this.deputyPerson = deputyPerson;
    }
  
}
