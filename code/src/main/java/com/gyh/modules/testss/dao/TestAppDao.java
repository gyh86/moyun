package com.gyh.modules.testss.dao;

import com.gyh.modules.testss.entity.TestApp;
import com.gyh.system.common.persistence.BaseDao;
import com.gyh.system.common.persistence.annotation.MyBatisDao;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
@MyBatisDao
public interface TestAppDao extends BaseDao<TestApp>{

}