package com.gyh.modules.testss.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.modules.testss.entity.App;
import com.gyh.modules.testss.service.AppService;
import com.gyh.system.common.utils.R;
import com.gyh.system.common.persistence.Page;
import com.gyh.system.common.persistence.BaseController;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
@RestController
@RequestMapping("/app")
public class AppController extends BaseController{
	
	@Autowired
	private AppService appService;
	
	/**
	 * 查询数据
	 * @return
	 */
	@GetMapping
	public R getList(App app) {
		List<App> apps = appService.findList(app);
		return R.ok(apps);
	}
	
	/**
	 * 分页查询数据
	 * @return
	 */
	@GetMapping("page")
	public R getList(App app,@RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
		Page<App> page = appService.findPage(new Page<App>(pageNo,pageSize), app);
		return R.ok(page);
	}
	
	/**
	 * 查询表中全部数据
	 * @return
	 */
	@GetMapping("getAll")
	public R getALLList(App app) {
		List<App> apps = appService.findList(app);
		return R.ok(apps);
	}
	
	/**
	 * 保存单条数据
	 * @return
	 */
	@PostMapping
	public R save(@RequestBody App app) {
		appService.save(app);
		return R.ok("保存成功");
	}
	
	/**
	 * 批量保存数据
	 * @return
	 */
	@PostMapping("save")
	public R saves(@RequestBody List<App> list) {
		for (App app : list) {
			appService.save(app);
		}
		return R.ok("保存成功");
	}
	
	/**
	 * 根据ID执行逻辑删除
	 * @return
	 */
	@GetMapping("delete")
	public R delete(Long id) {
		int rows = appService.delete(id);
		if (rows > 0) {
			return R.ok("删除成功");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
	/**
	 * 根据ID执行物理删除
	 * @return
	 */
	@GetMapping("remove")
	public R deleteAlways(Long id) {
		int rows = appService.deleteAlways(id);
		if (rows > 0) {
			return R.ok("已永久删除");
		}
		return R.ok(500,"未找到要删除的记录");
	}
	
}