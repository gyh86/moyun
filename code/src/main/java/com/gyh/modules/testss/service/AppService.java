package com.gyh.modules.testss.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.system.common.persistence.BaseService;
import com.gyh.modules.testss.dao.AppDao;
import com.gyh.modules.testss.entity.App;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
@Service
public class AppService extends BaseService<AppDao, App> {

}