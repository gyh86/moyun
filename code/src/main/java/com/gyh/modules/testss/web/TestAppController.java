package com.gyh.modules.testss.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gyh.modules.testss.entity.TestApp;
import com.gyh.modules.testss.service.TestAppService;
import com.gyh.system.common.utils.R;
import com.gyh.system.common.persistence.BaseController;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
@RestController
@RequestMapping("/testApp")
public class TestAppController extends BaseController{
	
	@Autowired
	private TestAppService testAppService;
	
	/**
	 * 查询数据
	 * @return
	 */
	@GetMapping
	public R getList(TestApp testApp) {
		List<TestApp> testApps = testAppService.findList(testApp);
		return R.ok(testApps);
	}
	
	/**
	 * 查询表中全部数据
	 * @return
	 */
	@GetMapping("getAll")
	public R getALLList(TestApp testApp) {
		List<TestApp> testApps = testAppService.findList(testApp);
		return R.ok(testApps);
	}
	
	/**
	 * 保存单条数据
	 * @return
	 */
	@PostMapping
	public R save(@RequestBody TestApp testApp) {
		testAppService.save(testApp);
		return R.ok("保存成功");
	}
	
	/**
	 * 批量保存数据
	 * @return
	 */
	@PostMapping("save")
	public R saves(@RequestBody List<TestApp> list) {
		for (TestApp testApp : list) {
			testAppService.save(testApp);
		}
		return R.ok("保存成功");
	}
	
	/**
	 * 根据ID执行逻辑删除
	 * @return
	 */
	@GetMapping("delete")
	public R delete(Long id) {
		testAppService.delete(id);
		return R.ok("删除成功");
	}
	
	/**
	 * 根据条件逻辑删除数据
	 * @return
	 */
//	@GetMapping("remove")
//	public R deletes(TestApp testApp) {
//		testAppService.delete(testApp);
//		return R.ok("删除成功");
//	}
	
	
}