package com.gyh.modules.testss.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
public class App extends BaseEntity<App>{

    private static final long serialVersionUID = 1L;

    private String name;		//名字
    private Integer age;		//年龄
    private Date createDate;		//创建时间
  
	
      
    public String getName() {
        return name;
    }
  
    public void setName(String name){
        this.name = name;
    }
      
    public Integer getAge() {
        return age;
    }
  
    public void setAge(Integer age){
        this.age = age;
    }
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")  
    public Date getCreateDate() {
        return createDate;
    }
  
    public void setCreateDate(Date createDate){
        this.createDate = createDate;
    }
  
}
