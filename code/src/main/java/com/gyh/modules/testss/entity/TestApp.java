package com.gyh.modules.testss.entity;

import java.util.Date;

import com.gyh.system.common.persistence.BaseEntity;

/**
* @author 作者 gyh
* @version 创建时间  $date
* 测试app数据
*/
public class TestApp extends BaseEntity<TestApp>{
  
  private static final long serialVersionUID = 1L;

      private String name;
      private Integer age;
      private Date createDate;
      	
	
    	    	    
    public String getName() {
      return name;
    }
    public void setName(String name){
      this.name = name;
    }
        	    
    public Integer getAge() {
      return age;
    }
    public void setAge(Integer age){
      this.age = age;
    }
        	    
    public Date getCreateDate() {
      return createDate;
    }
    public void setCreateDate(Date createDate){
      this.createDate = createDate;
    }
        	  
}
