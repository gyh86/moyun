<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>代码生成模块</title>
    <script src="gen/js/jquery.js"></script>
    <script src="gen/js/vue.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://unpkg.com/iview/dist/styles/iview.css">
    <script type="text/javascript" src="http://vuejs.org/js/vue.min.js"></script>
    <script type="text/javascript" src="http://unpkg.com/iview/dist/iview.min.js"></script>
</head>
<body>
    <div id="app">
        <div >
            <button style="margin-left: 50px;" v-on:click="gainData()" >加载数据库表</button>
            <button @click="nextSetting()" >下一步</button>
            <i-table highlight-row border @on-current-change="handleRowChange" :columns="columns1" :data="tables"></i-table>
        </div>
    </div>
    <script>
        var datum = "正在加载数据。。。。";
        new Vue({
            el: '#app',
            data: {
                tables:[{}],
                columns1:[
                    {
                        title: '名字',
                        key: 'jdbcName'
                    },
                    {
                        title: 'comments',
                        key: 'comments'
                    }],
                currentRow:{}
            },
            methods: {
                gainData: function () {
                    var _self=this;
                    $	.get('http://localhost:8080/moyun/table/database',function(result){
                        if (result.code == 0) {
                            _self.tables = result.data;
                        }
                    });
                },
                handleRowChange:function(currentRow, oldCurrentRow){
                   this.currentRow = currentRow;
                },
                nextSetting: function(){
                    var _self = this.currentRow;
                    if (_self != undefined) {
                    	window.location.href="http://localhost:8080/moyun/gen/columns.jsp?table="+_self.jdbcName;
                    } else {
                    	alert("请选择表");
                    }
                }
            }
        })
    </script>
</body>
</html>