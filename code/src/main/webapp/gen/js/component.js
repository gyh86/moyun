Vue.component('my-component',{
    template:'\
        <div>\
            <slot>\
                <p>如果父组件中没有插入数据，改数据将显示</p>\
            </slot>\
        </div>\
    ',
    data: function () {
        return {
            counter: 0
        }
    },
    methods: {
        handleIncrease: function () {
            this.counter++;
            this.$emit('increase',this.counter);
        },  
        handleReduce: function () {
            this.counter--;
            this.$emit('reduce',this.counter);
        }
    }
});

Vue.component('my-com',{
    props: ['value1'],
    template:'<input :value="value1" @input="updateValue">',
    methods: {
        updateValue: function () {
            this.$emit('input',this.value1);
        }
    }
});

var app1 = new Vue({
    el:'#app1',
    data: {
        total: 0
    },
    methods: {
        handleReduce: function () {
            this.total--;
        }
    }
});

var component2 = {
    props:['message'],
    template:'<div>局部模板:{{message}}</div>'
}

var app = new Vue({
    el:'#app',
    data: {
        message:'',
        total: 0
    },
    components:{
        'local-component':component2
    },
    computed: {
        totalPrice:function(){
            var total = 0;
            for (let i = 0; i < this.list.length; i++) {
               let item = this.list[i];
               total += item.price * item.count;
            }
            return total.toString().replace(/\B(?=(\d{3})+$)/g,',');
        }
    },
    methods:{
        handleReduce:function(index){
            if (this.list[index].count === 1) return;
            this.list[index].count -- ;
        },
        handleGetTotal: function (total) {
            this.total = total;
        }
    }
});
