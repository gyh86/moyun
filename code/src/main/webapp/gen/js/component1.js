Vue.component('my-com',{
    template:'<button @click="handleClick">+1</button>',
    data:function () {
        return {
            counter: 0
        }
    },
    methods: {
        handleClick: function () {
            this.counter++;
            this.$emit('input',this.counter);
        }
    }
});



var app1 = new Vue({
    el:'#app1',
    data: {
        total: 0
    },
    methods: {
        handleReduce: function () {
            this.total--;
        }
    }
});
